import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { InitCapsPipe } from './init-caps.pipe';

import { NavbarComponent } from './navbar/navbar.component';
import { SharedComponent } from './shared.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    InitCapsPipe,

    NavbarComponent,
     SharedComponent,
     FooterComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  exports:[
    NavbarComponent,
    FooterComponent
  ]
})
export class SharedModule { }
