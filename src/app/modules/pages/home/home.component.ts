import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
isShown:boolean=false;
  constructor(private route:Router) { }

  ngOnInit(): void {
  }
  viewMore()
  {
    return this.isShown=!this.isShown;
  }
  navigateToBusiness()
  {
    this.route.navigate(['./business/business-objective'])
  }
}

