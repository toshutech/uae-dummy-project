import { SharedModule } from './../../shared/shared.module';
import { PagesComponent } from './pages.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
// import { FormsModule, ReactiveFormsModule } from '@angular/forms';




@NgModule({
  declarations: [
  PagesComponent,
  // ReactiveFormsModule,
  // FormsModule
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule
  ]
})
export class PagesModule { }
