import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TabService } from 'src/app/providers/tab.service';

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.scss']
})
export class BusinessComponent implements OnInit {
  selectedValueInTab!: Number;
  oldValueStoreNo!: Number;
  currentRoute: any;
  isChecked1:boolean=false;
  isChecked2:boolean=false;
  isChecked3:boolean=false;
  isChecked4:boolean=false;
  isChecked6:boolean=false;
  isChecked5:boolean=false;

  checked:string='unchecked';
  constructor(public router: Router,
    public tabService: TabService) {
    this.selectedValueInTab = 1;

  }



  ngOnInit(): void {
    this.oldValueStoreNo = this.tabService.tabsNumber;
    this.onClickTabeGetValue(1)
  }

  onClickTabeGetValue(value: any) {
    console.log("this.tabService.tabsNumber ", this.tabService.tabsNumber)
    // if (this.tabService.tabsNumber == value) {
   if(value>1)
   {
     this.isChecked1=true;
   }
   if(value>2)
   {
     this.isChecked2=true;
   }
   if(value>3)
   {
     this.isChecked3=true;
   }
   if(value>4)
   {
     this.isChecked4=true;
   }
   if(value>5)
   {
     this.isChecked5=true;
   }
   if(value>6)
   {
     this.isChecked6=true;
   }



    this.selectedValueInTab = value;
    if (value == 1) {
      this.router.navigate(["/business/business-objective"])
    } else if (value == 2) {
      this.router.navigate(["/business/facilites"]);

    } else if (value == 3) {
      this.router.navigate(["/business/investor-info"]);

    }
    else if (value == 4) {
      this.router.navigate(["/business/business-name"]);

    }
    else if (value == 5) {
      this.router.navigate(["/business/required-doc"]);

    } else {
      this.router.navigate(["/business/summary"]);
    }
  }
  //  }
}
