import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FacilityRequiredService } from 'src/app/providers/facility-required.service';
import { TabService } from 'src/app/providers/tab.service';

interface FileUploadType {
  name: string;
  date: string;
  base64: string;
  type: string;
}

interface SelectedDocumentType {
  name: string;
  type: string;
  base64: string;
  index: number;
}
@Component({
  selector: 'app-required-document',
  templateUrl: './required-document.component.html',
  styleUrls: ['./required-document.component.scss'],
})
export class RequiredDocumentComponent implements OnInit {
  requiredDocumentList: any[] = [];
  uploadedDocuments: FileUploadType[] = [];
  blobFile: any[] = [];
  @ViewChild('modelButton') modalButton: ElementRef;

  selectedDocument: SelectedDocumentType = {
    name: '',
    type: '',
    base64: '',
    index: 0,
  };

  metaDataInfo:any = {

  }

  constructor(
    private sanitizer: DomSanitizer,
    private route: Router,
    private tabs: TabService,
    private apiService: FacilityRequiredService
  ) {}

  ngOnInit(): void {
    const routeToNavigate = this.tabs.navigateToPage()
    const currentRoute = window.location.pathname;
    if(routeToNavigate != currentRoute ){
      console.log("Navigation Called", routeToNavigate);
      this.route.navigate([routeToNavigate])
    }
    this.getRequiredDocuments();

    // this.populateData();

  }

  merge(): any[] {
    const list: any[] = [];
    this.requiredDocumentList.forEach((item, index) => {
      const obj = { ...item, ...this.uploadedDocuments[index] };
      list.push(obj);
    });
    return list;
  }

  save() {
    this.storeData();
    TabService.formValues.requiredDocuments = this.merge();
    sessionStorage.removeItem(TabService.localStorageKeys.globalDataKey);
    this.route.navigate([this.tabs.navigateToNextPage()]);
  }

  getRequiredDocuments() {
    this.apiService.getDocumentTypes().subscribe((res) => {
      if (Array.isArray(res)){
        this.requiredDocumentList = res.map((documentDetail)=>{
          documentDetail.metaData = documentDetail.metaData.split(',');
          this.metaDataInfo[documentDetail.documentName] = {}
          return documentDetail
        })
        this.populateData();
        console.log("Response", this.requiredDocumentList)
      } 
    });
  }

  deleteText(index: number) {
    this.blobFile[index] = undefined;
    this.uploadedDocuments[index] = {
      name: '',
      base64: '',
      date: '',
      type: '',
    };
  }

  base(event: any, index: number) {
    //if (this.userDetails != null) {
    const file = event.target.files[0];
    this.blobFile[index] = file;
    console.log(file.type);
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      console.log(reader.result);
      if (typeof reader.result === 'string' && typeof file.type === 'string') {
        this.uploadedDocuments[index] = {
          base64: reader.result,
          type: file.type,
          date: new Date().toISOString(),
          name: file.name,
        };
      }
    };

    //}
  }

  printPdf(base64: string) {
    //let json: any =  { "type":"Buffer", "data":this.blob }
    //let bufferOriginal = Buffer.from(json.data);
    const byteArray = new Uint8Array(
      atob(base64)
        .split('')
        .map((char) => char.charCodeAt(0))
    );
    const file = new Blob([byteArray], { type: 'application/pdf' });
    const fileURL = URL.createObjectURL(file);
    window.open(fileURL);
    return fileURL;
  }

  selectDocument(index: number) {
    console.log(this.requiredDocumentList[index]);
    const doc = { ...this.uploadedDocuments[index], date: undefined };
    this.selectedDocument = { ...doc, base64: doc.base64, index: index };
    if (doc.type == 'image/png' || doc.type == 'image/jpeg') {
      this.selectedDocument = { ...doc, base64: doc.base64, index: index };
      this.modalButton.nativeElement.click();
    } else if (doc.type == 'application/pdf') {
      this.selectedDocument = {
        ...doc,
        base64: this.printPdf(
          doc.base64.replace('data:application/pdf;base64,', '')
        ),
        index: index,
      };
    }
    // window.open(fileURL);
  }

  storeData() {
    const obj = {
      requiredDocumentList: this.requiredDocumentList,
      uploadedDocuments: this.uploadedDocuments,
      blobFile: this.blobFile,
      selectedDocument: this.selectedDocument,
    };
    sessionStorage.setItem(
      TabService.localStorageKeys.requiredDocument,
      JSON.stringify(obj)
    );
  }

  populateData() {
    const value = sessionStorage.getItem(
      TabService.localStorageKeys.requiredDocument
    );
    if (value) {
      const parsed = JSON.parse(value);
      this.requiredDocumentList = parsed.requiredDocumentList;
      this.uploadedDocuments = parsed.uploadedDocuments;
      this.blobFile = parsed.blobFile;
      this.selectedDocument = parsed.selectedDocument;
    }
  }
}
