import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessNameComponent } from './business-name.component';

describe('BusinessNameComponent', () => {
  let component: BusinessNameComponent;
  let fixture: ComponentFixture<BusinessNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessNameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
