import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { TabService } from 'src/app/providers/tab.service';
@Component({
  selector: 'app-business-name',
  templateUrl: './business-name.component.html',
  styleUrls: ['./business-name.component.scss']
})
export class BusinessNameComponent implements OnInit {
  businessNameForm!: FormGroup;



  constructor(private route: Router, public formBuilder: FormBuilder,private tabs:TabService) {
    const routeToNavigate = this.tabs.navigateToPage()
    const currentRoute = window.location.pathname;
    if(routeToNavigate != currentRoute ){
      this.route.navigate([routeToNavigate])
    }
   }

  ngOnInit(): void {

    this.businessNameForm = this.formBuilder.group({
      Preference1: ['', Validators.required],
      Preference2: ['', Validators.required],
      Preference3: ['', Validators.required],
    });

    this.getData();

  }
  get Preference1() { return this.businessNameForm.get('Preference1'); }

  get Preference2() { return this.businessNameForm.get('Preference2'); }

  get Preference3() { return this.businessNameForm.get('Preference3'); }
  businessNameFormSubmit() {
    console.log(this.businessNameForm.value)
    console.log("shsh", this.Preference1?.value)
    console.log(this.Preference2?.value)
    console.log(this.Preference3?.value)

  }
  save() {
    this.storeData();
    this.businessNameFormSubmit()
    console.log(this.businessNameForm.value)
    TabService.formValues.businessName = this.businessNameForm.value;
    this.route.navigate([this.tabs.navigateToNextPage()])
  }

  validateName(){
    const value = this.businessNameForm.value;
    const values = Object.values(value);

    const newList:any[] = [];
    let valid = true;
    values.forEach((text)=>{
      const findText = newList.find((i)=> i== text);
      if(findText) valid = false;
      else newList.push(text)
    })

    return !valid
  }

  storeData(){
    const obj = { businessNameForm: this.businessNameForm.value }
    sessionStorage.setItem(TabService.localStorageKeys.businessName, JSON.stringify(obj));
  }

  getData(){
    const data = sessionStorage.getItem(TabService.localStorageKeys.businessName)
    if(data){
      const parsed = JSON.parse(data);
      this.businessNameForm.patchValue({ ...parsed.businessNameForm })
    }
  }

}
