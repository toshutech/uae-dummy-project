import { LocationStrategy } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FacilityRequiredService } from 'src/app/providers/facility-required.service';
import {
  BusinessObjective,
  FormValueTypes,
  InvestorInformation,
  TabService,
} from 'src/app/providers/tab.service';

interface SetupDetailsType {
  costType: string;
  cost: number;
  currency: string;
}

@Component({
  selector: 'app-summary-and-payment',
  templateUrl: './summary-and-payment.component.html',
  styleUrls: ['./summary-and-payment.component.scss'],
})
export class SummaryAndPaymentComponent implements OnInit {
  allFormValue: FormValueTypes;
  businessObjective: BusinessObjective;
  businessName: any;
  facilitiesRequired: any[];
  investorInformation: InvestorInformation;
  requiredDocuments: any;

  legalZone: string = '';
  visaRequired: number = 0;
  investorVisaRequired: number = 0;

  setupDetails: SetupDetailsType[] = [];
  typeOfLicense: any = {};
  legalStructure: string = '';

  noOfInvestorVisaRequired: number = 0;
  visaCosting: any = {
    investorVisa: 0,
    employeeVisa: 0,
  };

  originalVisaCosting: any = {
    employee: 0,
    investor: 0,
  };

  constructor(
    private tabService: TabService,
    private apiService: FacilityRequiredService,
    private router: Router,
    private locationStratgey: LocationStrategy
  ) {
    const value: FormValueTypes = TabService.formValues;

    console.log('FormValueTypeValue', value);

    this.allFormValue = value;
    this.businessName = value.businessName;
    this.businessObjective = value.businessObjective;
    this.facilitiesRequired = value.facilitiesRequired;
    this.investorInformation = value.investorInformation;
    this.requiredDocuments = value.requiredDocuments;

    const firstValueOfFacilites = value.facilitiesRequired[0];
    if (firstValueOfFacilites) {
      this.legalZone = firstValueOfFacilites.officeType;
      this.visaRequired = firstValueOfFacilites.noOfVisa;

      let investorVisa = 0;
      this.investorInformation.investors.forEach((item: any) =>
        item.visa == 'Yes' ? (investorVisa += 1) : ''
      );
    }
  }

  ngOnInit(): void {
    history.pushState(null, null, window.location.href);
    this.locationStratgey.onPopState(() => {
      history.pushState(null, null, window.location.href);
    });

    // this.populateData()
    this.totalFacilityCost();
    this.setupCost();
    console.log('Form values', TabService.formValues);
    // this.typeOfLicense = TabService.formValues.facilitiesRequired[0].officeType;
    this.legalStructure = TabService.formValues.investorInformation.partnership;
    this.getInvestorVisaRequired();
    this.getEmployeeVisaRequired();
    this.getLicenseType();
    this.fetchVisaCostOfInvestorEmployee();
    // this.populateData()
    console.log('TabServiceFacilityRequired', TabService.formValues);
  }

  fetchVisaCostOfInvestorEmployee() {
    if (Array.isArray(this.facilitiesRequired) && this.facilitiesRequired[0]) {
      const first = this.facilitiesRequired[0];
      if (first.officeType.toLowerCase() === 'freezone') {
        this.apiService
          .getEmployeeInvestorVisaCost(
            'regulatoryAuthority',
            first.zoneType,
            first.emirate
          )
          .subscribe((res: any) => {
            console.log('FInalCosting', res);
            this.originalVisaCosting = {
              employee: res.employeeVisa,
              investor: res.investorVisa,
            };
            this.visaCosting = {
              investorVisa: res.investorVisa,
              employeeVisa: res.employeeVisa,
            };
          });
      } else {
        this.apiService
          .getEmployeeInvestorVisaCost(
            'mainland',
            first.zoneType,
            first.emirate
          )
          .subscribe((res: any) => {
            console.log('FInalCosting', res);
            this.originalVisaCosting = {
              employee: res.employeeVisa,
              investor: res.investorVisa,
            };
            this.visaCosting = {
              investorVisa: res.investorVisa,
              employeeVisa: res.employeeVisa,
            };
          });
      }
    }
  }

  getEmployeeCostingToRender() {}

  getInvestorCostingToRender() {}

  getLicenseType() {
    const facilitiesRequired = this.facilitiesRequired[0];
    if (facilitiesRequired) {
      this.apiService
        .getLicenseCost({
          emirate: facilitiesRequired.emirate,
          zoneType: facilitiesRequired.zoneType,
          spaceType: facilitiesRequired.officeType,
        })
        .subscribe((res) => {
          this.typeOfLicense = res;
        });
    }
  }

  getInvestorVisaRequired() {
    console.log('This Investor Information', this.investorInformation);
    this.investorInformation.investors.forEach((investor: any) => {
      if (investor.visa) this.noOfInvestorVisaRequired += 1;
    });
  }

  getEmployeeVisaRequired() {
    this.facilitiesRequired.forEach((i) => {
      this.visaRequired = i.noOfVisa;
      this.visaCosting.employeeVisa = i.employeeVisa;
      this.visaCosting.investorVisa = i.investorVisa;
    });
  }

  navigateToFacilityRequired(step: number) {
    const navigate = { isStep: step };
    sessionStorage.setItem('facilityEditMode', JSON.stringify(navigate));
  }

  totalFacilityCost(): number {
    let cost = 0;
    this.facilitiesRequired.forEach((item: any) => (cost += item.cost));
    return cost
  }

  setupCost(): void {
    this.setupDetails = [];
    this.facilitiesRequired.forEach((tableData) => {
      const cityName = tableData.emirate;
      const spaceType = tableData.officeType;
      const spaceTypeName = tableData.zoneType;

      this.apiService
        .getSetupDetails({ cityName, spaceType, spaceTypeName })
        .subscribe((res) => {
          if (Array.isArray(res)) {
            res.forEach((i) => this.setupDetails.push(i));
          }
        });
    });
  }

  findSetupDetails(costType: string): number {
    const result = this.setupDetails.find(
      (item) => item.costType.toLowerCase() == costType.toLowerCase()
    );
    this.storeData();
    if (result) return result.cost;
    else return 0;
  }

  goToPage(tabNum: number, step: number = 1) {
    const routeString = this.tabService.navigateToParticularPage(tabNum);
    // if(tabNum === 1) sessionStorage.setItem("facilityEditMode", String(step));
    sessionStorage.setItem(
      TabService.localStorageKeys.activeTabIndex,
      String(step)
    );
    this.router.navigate([routeString]);
  }

  totalSetupCost(): number {
    let cost = 0;
    this.setupDetails.forEach((item) => (cost += item.cost));
    this.storeData();

    return cost;
  }

  overallCosting() {
    const total = this.totalFacilityCost() + this.totalSetupCost() +  (this.noOfInvestorVisaRequired * this.visaCosting.investorVisa) +
    (   this.visaRequired * this.visaCosting.employeeVisa )+
       this.typeOfLicense.cost
    this.storeData();

    return total;
  }

  storeData() {
    const obj = {
      allFormValue: this.allFormValue,
      businessObjective: this.businessObjective,
      businessName: this.businessName,
      facilitiesRequired: this.facilitiesRequired,
      investorInformation: this.investorInformation,
      requiredDocuments: this.requiredDocuments,

      legalZone: this.legalZone,
      visaRequired: this.visaRequired,
      investorVisaRequired: this.investorVisaRequired,

      setupDetails: this.setupDetails,
      typeOfLicense: this.typeOfLicense,
      legalStructure: this.legalStructure,

      noOfInvestorVisaRequired: this.noOfInvestorVisaRequired,
      visaCosting: this.visaCosting,
    };
    sessionStorage.setItem('SummaryPage', JSON.stringify(obj));
  }

  populateData() {
    const value = sessionStorage.getItem('SummaryPage');
    if (value) {
      const parsed = JSON.parse(value);

      this.allFormValue = parsed.allFormValue;
      this.businessObjective = parsed.businessObjective;
      this.businessName = parsed.businessName;
      this.facilitiesRequired = parsed.facilitiesRequired;
      this.investorInformation = parsed.investorInformation;
      this.requiredDocuments = parsed.requiredDocuments;

      this.legalZone = parsed.legalZone;
      this.visaRequired = parsed.visaRequired;
      this.investorVisaRequired = parsed.investorVisaRequired;

      this.setupDetails = parsed.setupDetails;
      this.typeOfLicense = parsed.typeOfLicense;
      this.legalStructure = parsed.legalStructure;

      this.noOfInvestorVisaRequired = parsed.noOfInvestorVisaRequired;
      this.visaCosting = parsed.visaCosting;
    }
  }
}
