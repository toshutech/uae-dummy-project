import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessObjectiveComponent } from './business-objective.component';

describe('BusinessObjectiveComponent', () => {
  let component: BusinessObjectiveComponent;
  let fixture: ComponentFixture<BusinessObjectiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessObjectiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessObjectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
