import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { INITIAL_STATE, TabService } from 'src/app/providers/tab.service';
import { BusinessActivityCategoryService } from 'src/app/providers/business-activity-category.service';
@Component({
  selector: 'app-business-objective',
  templateUrl: './business-objective.component.html',
  styleUrls: ['./business-objective.component.scss'],
})
export class BusinessObjectiveComponent implements OnInit {
  selectedValue: any;
  isStep: number = 1;
  businessObjective!: FormGroup;
  businessActivityStoreArray: any = [];
  buttonDisable = true;
  businessActivityCategoryList: any[];
  businessCategory: any[];

  selectedBusinessCategory: string = '';
  selectAllModel: boolean = false;

  formValues: any[] = [];

  // businessActivity = [
  //   {
  //     id: 1,
  //     title: "Business activity name",
  //     description1: "Description Description Description Description Description Description Description",
  //     description2: "Any mandatory documentation/approvals needed",
  //     description3: "cost"
  //   },
  //   {
  //     id: 2,
  //     title: "Business activity name",
  //     description1: "Description Description Description Description Description Description Description",
  //     description2: "Any mandatory documentation/approvals needed",
  //     description3: "cost"
  //   },
  //   {
  //     id: 3,
  //     title: "Business activity name",
  //     description1: "Description Description Description Description Description Description Description",
  //     description2: "Any mandatory documentation/approvals needed",
  //     description3: "cost"
  //   },
  //   {
  //     id: 4,
  //     title: "Business activity name",
  //     description1: "Description Description Description Description Description Description Description",
  //     description2: "Any mandatory documentation/approvals needed",
  //     description3: "cost"
  //   },
  //   {
  //     id: 5,
  //     title: "Business activity name",
  //     description1: "Description Description Description Description Description Description Description",
  //     description2: "Any mandatory documentation/approvals needed",
  //     description3: "cost"
  //   },
  //   {
  //     id: 6,
  //     title: "Business activity name",
  //     description1: "Description Description Description Description Description Description Description",
  //     description2: "Any mandatory documentation/approvals needed",
  //     description3: "cost"
  //   },
  // ]
  constructor(
    private route: Router,
    private businessActivityCategoryService: BusinessActivityCategoryService,
    private tabs: TabService
  ) {
    this.businessActivityCategoryList = [];
    this.businessCategory = [];
  }

  ngOnInit(): void {
    this.getBusinessActivityCategoryList();

    const routeToNavigate = this.tabs.navigateToPage();
    const currentRoute = window.location.pathname;
    if (routeToNavigate != currentRoute) {
      console.log('Navigation Called', routeToNavigate);
      this.route.navigate([routeToNavigate]);
    }
    this.checkForEditMode();
  }

  checkForEditMode() {
    const index = sessionStorage.getItem(
      TabService.localStorageKeys.activeTabIndex
    );
    if (index) {
      this.isStep = Number(index);
      if (this.isStep === 1) {
        TabService.formValues = INITIAL_STATE;
        sessionStorage.removeItem(TabService.localStorageKeys.facilitiesRequired);
        sessionStorage.removeItem(TabService.localStorageKeys.investorInformation);
        sessionStorage.removeItem(TabService.localStorageKeys.requiredDocument);
        sessionStorage.removeItem(TabService.localStorageKeys.businessName);
        sessionStorage.removeItem(TabService.localStorageKeys.globalDataKey);
      } else {
        
      }
    }
    console.log("Index", this.isStep)
    this.getData();
  }

  navigate() {
    this.isStep -= 1;
  }

  getBusinessCategoryByCategoryName(value: any) {

    console.log("Value", "APi Called")
    this.selectedBusinessCategory = value;
    this.businessActivityCategoryService
    .getBusinessCategoryByCategoryName(value)
    .subscribe(
      (res: any) => {
          console.log("Value", "APi Called", res)
          console.log(res);
          this.selectedValue = value;
          this.isStep = 2;
          this.businessCategory = res;
        },
        (err) => {
          console.log(err);
        }
      );
      this.storeData()
  }

  getBusinessActivityCategoryList() {
    this.businessActivityCategoryService.getAllBusiness().subscribe((res) => {
      if (Array.isArray(res)) {
        this.businessActivityCategoryList = res;
        console.log(this.businessActivityCategoryList);
      }
    });
  }

  onCheckboxChange(id: any, e: any) {
    if (e.target.checked) {
      this.businessCategory.forEach((ele) => {
        if (ele._id == id) {
          this.businessActivityStoreArray.push(ele);
        }
      });
    } else {
      this.businessActivityStoreArray.forEach((ele: any, index: any) => {
        if (ele._id == id) {
          this.businessActivityStoreArray.splice(index, 1);
        }
      });
    }
    if (this.businessActivityStoreArray.length >= 1) {
      this.buttonDisable = false;
    } else {
      this.buttonDisable = true;
    }
    console.log(this.businessActivityStoreArray);
    this.validateAllBusinessActivitySelected();
    this.storeData();
  }

  validateAllBusinessActivitySelected(){ //specific for business Activity
    console.log("Busssiness Category Form Value:", this.businessCategory, this.formValues);
    
    const totalArrayLength = this.businessCategory.length;
    let totalSelectedValue = 0;

    this.formValues.forEach((i)=>{
      if(i) totalSelectedValue += 1
    })
    this.selectAllModel = this.businessCategory.length === totalSelectedValue;
  }

  getValue(value: any) {}
  save() {
    this.storeData();
    TabService.formValues.businessObjective.businessActivityCategory =
      this.selectedBusinessCategory;
    TabService.formValues.businessObjective.businessActivity =
      this.businessActivityStoreArray;

    sessionStorage.setItem(
      TabService.localStorageKeys.businessActivity,
      JSON.stringify({
        businessActivity: this.businessActivityStoreArray,
        businessActivityCategory: this.selectedBusinessCategory,
      })
    );
    if (this.isStep + 1 > 2) {
      this.route.navigate(['/business/facilites']);
      this.isStep = 1;
    } else {
      this.isStep += 1;
    }
    this.route.navigate([this.tabs.navigateToNextPage()]);
  }

  storeData() {
    const obj = {
      selectedValue: this.selectedValue,
      isStep: this.isStep,
      businessActivityStoreArray: this.businessActivityStoreArray,
      buttonDisable: this.buttonDisable,
      businessActivityCategoryList: this.businessActivityCategoryList,
      businessCategory: this.businessCategory,
      selectedBusinessCategory: this.selectedBusinessCategory,
      formValues: this.formValues,
    };
    console.log("Selected Store Data", obj)
    sessionStorage.setItem(
      TabService.localStorageKeys.businessObjective,
      JSON.stringify(obj)
    );
  }

  getData() {
    const data = sessionStorage.getItem(
      TabService.localStorageKeys.businessObjective
    );
    if (data) {
      const parsed = JSON.parse(data);
      console.log('Parsed Get Data', parsed);
      this.selectedValue = parsed.selectedValue;
      this.isStep = Number(parsed.isStep);

      this.businessActivityStoreArray = parsed.businessActivityStoreArray;
      this.buttonDisable = parsed.buttonDisable;
      this.businessActivityCategoryList = parsed.businessActivityCategoryList;
      this.businessCategory = parsed.businessCategory;
      this.selectedBusinessCategory = parsed.selectedBusinessCategory;
      this.formValues = parsed.formValues;
      this.getBusinessCategoryByCategoryName(this.selectedBusinessCategory);
      console.log("IsTep", this.isStep)
    }
  }

  handleSelectAll(event:any){
    const checked = event.target.checked;
    this.businessActivityStoreArray = []

    this.businessCategory.forEach((i, index)=>{
      this.formValues[index] = checked;
      if(checked){
        this.businessActivityStoreArray.push(i)
      } else {
        this.businessActivityStoreArray = []
      }
      // this.businessActivityStoreArray[index] = 
    });

    this.buttonDisable = !checked;

  }
}
