import { BusinessNameComponent } from './business-name/business-name.component';
import { SummaryAndPaymentComponent } from './summary-and-payment/summary-and-payment.component';
import { RequiredDocumentComponent } from './required-document/required-document.component';
import { InvestorInformationComponent } from './investor-information/investor-information.component';
import { FacilitiesRequiredComponent } from './facilities-required/facilities-required.component';
import { BusinessObjectiveComponent } from './business-objective/business-objective.component';
import { BusinessComponent } from './business.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path:'',
    component:BusinessComponent,
    children:[
      {
        path:'',
        redirectTo:'business-objective',
        pathMatch:'full'
      },
      {
        path:'business-objective',
        component:BusinessObjectiveComponent
      },
      {
        path:'facilites',
        component:FacilitiesRequiredComponent
      },
      {
        path:'investor-info',
        component:InvestorInformationComponent
      },
      {
        path:'required-doc',
        component:RequiredDocumentComponent
      },
      {
        path:'business-name',component:BusinessNameComponent
      },
      {
        path:'summary',
        component:SummaryAndPaymentComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BusinessRoutingModule { }
