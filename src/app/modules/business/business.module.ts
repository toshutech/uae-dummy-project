import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BusinessRoutingModule } from './business-routing.module';
import { BusinessComponent } from './business.component';
import { BusinessObjectiveComponent } from './business-objective/business-objective.component';
import { FacilitiesRequiredComponent } from './facilities-required/facilities-required.component';
import { InvestorInformationComponent } from './investor-information/investor-information.component';
import { RequiredDocumentComponent } from './required-document/required-document.component';
import { SummaryAndPaymentComponent } from './summary-and-payment/summary-and-payment.component';
import { BusinessNameComponent } from './business-name/business-name.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    BusinessComponent,
    BusinessObjectiveComponent,
    FacilitiesRequiredComponent,
    InvestorInformationComponent,
    BusinessNameComponent,

    RequiredDocumentComponent,
    SummaryAndPaymentComponent,



  ],
  imports: [
    CommonModule,
    BusinessRoutingModule,
    SharedModule, FormsModule,PdfViewerModule,
    ReactiveFormsModule,
  ]
})
export class BusinessModule { }
