import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControlName,
  FormControl,
  Validators,
  FormArray,
  FormBuilder,
} from '@angular/forms';
import { Router } from '@angular/router';
import { FacilityRequiredService } from 'src/app/providers/facility-required.service';
import { INITIAL_STATE, TabService } from 'src/app/providers/tab.service';

class InvestorFormObjType {
  name: string = '';
  equityPercent: number = 100;
  nationality: string = '';
  profitPercent: number = 100;
  amount: number = 0;
  visa: boolean = false;
}

@Component({
  selector: 'app-investor-information',
  templateUrl: './investor-information.component.html',
  styleUrls: ['./investor-information.component.scss'],
})
export class InvestorInformationComponent implements OnInit {
  addFields!: FormArray;
  investorList: any[] = [];
  investorForm: InvestorFormObjType[] = []; // We are using this for multiple Form Obj.

  additionalDetailsArray!: FormArray;
  partnershipList: any[] = [];
  partnershipType: string = "";

  constructor(
    private route: Router,
    private fb: FormBuilder,
    private tabs: TabService,
    private apiService: FacilityRequiredService
  ) {}

  investerStepOneForm = new FormGroup({
    additionalDetailsArray: new FormArray([]),
  });

  submitInvesterStepOneForm() {
    console.log(this.investerStepOneForm.value);
  }

  calculatePercentage(key: string): number {
    const value = this.investorForm;
    let totalPercentage: number = 0;
    if (Array.isArray(value)) {
      value.forEach((formValue) => {
        totalPercentage += formValue[key];
      });
    }
    return totalPercentage;
  }

  createAditionalDetails(value: number = 0): FormGroup {
    const equityPercent =
      this.calculatePercentage('equityPercent') > 0
        ? 100 - this.calculatePercentage('equityPercent')
        : 100;

    const profitPercent =
      this.calculatePercentage('profitPercent') > 0
        ? 100 - this.calculatePercentage('profitPercent')
        : 100;

    return this.fb.group({
      name: new FormControl('', Validators.required),
      profitPercent: new FormControl(
        value > 0 ? value : profitPercent,
        Validators.required
      ),
      equityPercent: new FormControl(
        value > 0 ? value : equityPercent,
        Validators.required
      ),
      nationality: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      visa: new FormControl(true, Validators.required),
    });
  }
  addAditionalDetails(value: number = 0): void {
    const newObj = new InvestorFormObjType();

    let totalEquityPercentage = 0;
    let totalProfitPercentage = 0;
    this.investorForm.forEach((i) => {
      totalEquityPercentage += i.equityPercent;
      totalProfitPercentage += i.profitPercent;
    });

    newObj.profitPercent = 100 - totalEquityPercentage;
    newObj.equityPercent = 100 - totalProfitPercentage;

    this.investorForm.push(newObj);
    // this.additionalDetailsArray.push(this.createAditionalDetails(value));
  }

  investerStepTwoForm = new FormGroup({
    partnership: new FormControl('', Validators.required),
  });
  submitInvesterStepTwoForm() {
    console.log(this.investerStepTwoForm.value);
  }

  investerStepThreeForm = new FormGroup({
    amount: new FormControl('', Validators.required),
  });
  submitInvesterStepThreeForm() {
    console.log(this.investerStepThreeForm.value);
  }
  isStep: number = 1;

  ngOnInit(): void {
    const routeToNavigate = this.tabs.navigateToPage();
    const currentRoute = window.location.pathname;
    if (routeToNavigate != currentRoute) {
      console.log('Navigation Called', routeToNavigate);
      this.route.navigate([routeToNavigate]);
    }
    this.additionalDetailsArray = this.investerStepOneForm.get(
      'additionalDetailsArray'
    ) as FormArray;
    this.addAditionalDetails(100);
    this.getPartnershipList();
    this.getData();
    this.checkStep();
  }

  checkStep() {
    const isStep = sessionStorage.getItem(
      TabService.localStorageKeys.activeTabIndex
    );
    console.log("Isstedslkdp", isStep);
    if (Number(isStep) === 1) {
      TabService.formValues.investorInformation =
      INITIAL_STATE.investorInformation;
      TabService.formValues.requiredDocuments = INITIAL_STATE.requiredDocuments;
      TabService.formValues.businessName = {};
      TabService.formValues.investorInformation.partnership= "";

      this.getData();
      this.partnershipType = "";
      sessionStorage.removeItem(TabService.localStorageKeys.investorInformation)

      this.isStep = 1
      
      sessionStorage.removeItem(TabService.localStorageKeys.activeTabIndex)
      sessionStorage.removeItem(TabService.localStorageKeys.globalDataKey)
      sessionStorage.removeItem(TabService.localStorageKeys.businessName)
    }
  }

  save() {
    this.storeData();
    if (
      this.calculatePercentage('equityPercent') > 100 ||
      this.calculatePercentage('equityPercent') < 100
    )
      return;
    if (
      this.calculatePercentage('profitPercent') > 100 ||
      this.calculatePercentage('profitPercent') < 100
    )
      return;
    this.isStep += 1;
    this.tabs.tabsNumber = this.isStep;
    console.log(this.investerStepThreeForm.value);
    TabService.formValues.investorInformation.investors =
      this.investorForm;
    TabService.formValues.investorInformation.partnership =
      this.partnershipType;
    TabService.formValues.investorInformation.amount =
      this.investerStepThreeForm.value.amount;;

    console.log("InformationInformation", TabService.formValues.investorInformation)
    this.route.navigate([this.tabs.navigateToNextPage()]);
    // this.route.navigate(['/business/business-name']);
  }
  step1() {
    this.isStep += 1;
    this.tabs.tabsNumber = this.isStep;
    this.storeData();
    console.log('Stepper Form Values', this.investerStepOneForm.value);
  }
  step2() {
    this.isStep += 1;
    this.tabs.tabsNumber = this.isStep;
    this.investorList = this.investerStepOneForm.value.additionalDetailsArray;
    this.storeData();
  }

  getPartnershipList() {
    this.apiService.getPartnershipType().subscribe((res) => {
      if (Array.isArray(res)) this.partnershipList = res;
      console.log('This . Partnership', res);
    });
  }

  getPartnerTypeValue() {
    return !(this.investerStepTwoForm?.value.partnership.length > 0);
  }

  getAmountValue() {
    if (this.investerStepThreeForm?.value.amount > 0) return false;
    return true;
  }

  handlePartnerTypeChange(changeEvent:any){
    console.log("Partner Type", this.partnershipType);
    this.storeData();
  }

  storeData() {
    const obj = {
      investorForm: this.investorForm,
      isStep: this.isStep,
      partnershipType: this.partnershipType
    };
    sessionStorage.setItem(
      TabService.localStorageKeys.investorInformation,
      JSON.stringify(obj)
    );
  }

  getData() {
    const value = sessionStorage.getItem(
      TabService.localStorageKeys.investorInformation
    );
    if (value) {
      const parsed = JSON.parse(value);
      this.isStep = parsed.isStep;
      this.investorForm = parsed.investorForm;
      this.partnershipList = parsed.partnershipList;
      this.partnershipType = parsed.partnershipType;
      this.getPartnershipList();
    }
  }
}
