import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { FacilityRequiredService } from 'src/app/providers/facility-required.service';
import { INITIAL_STATE, TabService } from 'src/app/providers/tab.service';

interface TableCheckboxesType {
  main: boolean;
  children: any[];
}

@Component({
  selector: 'app-facilities-required',
  templateUrl: './facilities-required.component.html',
  styleUrls: ['./facilities-required.component.scss'],
})
export class FacilitiesRequiredComponent implements OnInit {
  isStep: number = 1;
  tableData: any[] = [];

  selectEmirate!: FormGroup;
  SelectLocationType!: FormGroup;
  SelectZoneType!: FormGroup;
  SelectSpaceype!: FormGroup;
  SelectSpaceArea!: FormGroup;
  selectNoEmployee!: FormGroup;
  selectFromTable!: FormGroup;
  locationByCityList: any;
  getLocationList: any[];
  cityList: any[];
  show: any = 0;
  spaceTypeValues: any[] = [];
  businessObjective: any = {};

  spaceType: Array<any> = [
    { name: 'Warehouse', value: 'Warehouse' },
    { name: 'Office', value: 'Office' },
    { name: 'Plot', value: 'Plot' },
  ];
  companyArea: Array<any> = [];
  visaType: Array<any> = [
    { name: 'yes', value: 'yes' },
    { name: 'No', value: 'No' },
  ];
  checkAllArrayValues: any[] = [];
  checkAllLocationValues: any[] = [];
  checkAllSpaceCheckedValues: any[] = [];

  locationList: any[] = [];
  zoneList: any[] = [];

  facilityTypeCosting: any[] = [];

  spaceTypeArray!: any[];
  searchText: string = '';
  filteredTableData: any[] = [];

  tableCheckboxes: TableCheckboxesType = {
    main: false,
    children: [],
  };
  visaCosting: any[] = [];

  showDetails: any[] = [];

  // select all;
  emirateSelectedAll: boolean = false;
  freeZoneMainLandSelectedAll: boolean = false;

  constructor(
    private fb: FormBuilder,
    private facilityRequiredService: FacilityRequiredService,
    private route: Router,
    private tabs: TabService
  ) {
    this.cityList = [];
    this.getLocationList = [];
    this.locationByCityList = [];
    this.selectEmirate = this.fb.group({
      checkArray: this.fb.array([]),
    });
    this.SelectLocationType = this.fb.group({
      locationType: ['', Validators.required],
    });
    this.SelectZoneType = this.fb.group({
      zoneArray: this.fb.array([]),
    });

    this.SelectSpaceype = this.fb.group({
      spaceArray: this.fb.array([], [Validators.required]),
    });

    this.SelectSpaceArea = this.fb.group({
      spaceTypeForOffice: [''],
      spaceTypeForWarehouse: [''],
      spaceTypeForPlot: [''],
    });

    this.selectNoEmployee = this.fb.group({
      noOfEmployee: ['', [Validators.required]],
      visaRequired: ['', [Validators.required]],
      noOfVisa: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    const routeToNavigate = this.tabs.navigateToPage();
    const currentRoute = window.location.pathname;
    if (routeToNavigate != currentRoute) {
      this.route.navigate([routeToNavigate]);
    }
    this.populateData();
    this.allCity();
    this.getLocation();
    this.getFacilityTypeList();
    this.checkStep();
  }

  checkStep() {
    const isStep = sessionStorage.getItem(
      TabService.localStorageKeys.activeTabIndex
    );
    if (isStep) {
      this.isStep = Number(isStep);
      console.log(this.isStep);
      if (Number(isStep) === 7) {
        sessionStorage.removeItem(
          TabService.localStorageKeys.investorInformation
        );
        sessionStorage.removeItem(TabService.localStorageKeys.businessName);
        sessionStorage.removeItem(TabService.localStorageKeys.requiredDocument);

        TabService.formValues.businessName = INITIAL_STATE.businessName;
        TabService.formValues.investorInformation =
          INITIAL_STATE.investorInformation;
        TabService.formValues.requiredDocuments =
          INITIAL_STATE.requiredDocuments;
      }
      if (Number(isStep) === 6) {
        this.isStep = 6;
        sessionStorage.removeItem(
          TabService.localStorageKeys.investorInformation
        );
        sessionStorage.removeItem(TabService.localStorageKeys.businessName);
        sessionStorage.removeItem(TabService.localStorageKeys.requiredDocument);

        TabService.formValues.businessName = INITIAL_STATE.businessName;
        TabService.formValues.investorInformation =
          INITIAL_STATE.investorInformation;
        TabService.formValues.requiredDocuments =
          INITIAL_STATE.requiredDocuments;
      }
      console.log('isStep', isStep);
    }
  }

  showDiv(id: any) {
    this.show = id;
    console.log(id);
  }

  navigate() {
    this.isStep -= 1;
  }

  getLocationByCityList(values: any[]) {
    this.locationByCityList = [];
    values.forEach((value) => {
      this.facilityRequiredService.getLocationByCity(value).subscribe(
        (res: any) => {
          console.log('Res', res);
          this.locationByCityList.push(...res);
        },
        (err) => {
          console.log(err);
        }
      );
    });
  }

  // takes emirate return location list. called in case of mainland
  getLocationListByEmirate(value: string) {
    const findCity = this.locationList.find(
      (item) => item.name.toLowerCase() === value.toLowerCase()
    );
    if (findCity) return findCity?.cities;
    else return [];
  }

  //filter zone base on emirate. takes emirate and returns array of zone. called in case of freezone
  getZoneListByEmirate(value: string) {
    const filterFreeZone = this.cityList.filter((city) => {
      const zone = city.zone.toLowerCase();
      return zone === 'freezone';
    });

    const filterMainLand = this.cityList.filter((city) => {
      return city.zone.toLowerCase() === 'mainland';
    });

    let arr = [];
    if (this.selectedZoneType.freezone && this.selectedZoneType.mainland) {
      arr = filterFreeZone.concat(filterMainLand);
    } else if (this.selectedZoneType.freezone) {
      arr = filterFreeZone.concat([]);
    } else if (this.selectedZoneType.mainland) {
      arr = [].concat(filterMainLand);
    }

    console.log('Filter By Zone', arr);

    const findZone = arr.find(
      //ZoneName  key of zone Name
      (item) => item.CityName.toLowerCase() === value.toLowerCase() && item.zone
    );
    if (findZone) return findZone?.place.map((str) => ({ ZoneName: str }));
    else return [];
  }

  getEmirateName(location: string, array: any[], type = 'cities') {
    const findLocation = array.find((item) => {
      console.log('FInd Location', location);
      let find: any;
      if (type == 'cities') {
        find = item[type].find((i: any) => i.locationName === location);
      }
      if (type == 'zones') {
        find = item[type].find((i: any) => i.ZoneName === location);
      }
      console.log('FInd Cities', find);
      if (find) return true;
      else return false;
    });
    if (findLocation) return findLocation.name;
    else return '';
  }

  isCityChecked(city: string) {
    return -1 < this.checkAllArrayValues.indexOf(city);
  }

  isLocationChecked(location: string) {
    return -1 < this.checkAllLocationValues.indexOf(location);
  }

  isSpaceChecked(location: string) {
    return -1 < this.checkAllSpaceCheckedValues.indexOf(location);
  }
  getLocationListByCityName(city: string): any[] {
    return this.locationByCityList.filter(
      (item: any) => item.cityName === city
    );
  }

  titleCase(str: string) {
    const first = str[0].toUpperCase();
    return first + str.slice(1, str.length);
  }

  allCity() {
    const data = sessionStorage.getItem(
      TabService.localStorageKeys.businessActivity
    );
    if (data) {
      const businessActivity = JSON.parse(data);
      console.log('Bussiness Objective', businessActivity);
      //  TabService.formValues.businessObjective;
      this.facilityRequiredService
        .getEmiratesBasedOfActivity(businessActivity)
        .subscribe((res) => {
          if (Array.isArray(res)) {
            this.cityList = res;
            console.log('City List ----------------', this.cityList);
            this.checkAllEmiratesSelected();
            this.populateSpaceType();
          }
        });
    }
  }
  getLocation() {
    this.facilityRequiredService.getAllFacilityLocation().subscribe((res) => {
      if (Array.isArray(res)) {
        this.getLocationList = res;
        console.log('Location List', this.getLocationList);
      }
    });
  }

  get noOfEmployee() {
    return this.selectNoEmployee.get('noOfEmployee');
  }

  get visaRequired() {
    return this.selectNoEmployee.get('visaRequired');
  }

  get noOfVisa() {
    return this.selectNoEmployee.get('noOfVisa');
  }

  step6() {
    this.isStep += 1;

    const obj = {
      selectedEmirateZones: this.selectedEmirateZones,
      selectedEmirates: this.selectedEmirates,
      selectedZoneType: this.selectedZoneType,
      W: TabService.facilitiesRequired.spaceTypeValues, //W stands for Warehouse Office etc
      area: this.SelectSpaceArea.value,
      noOfEmployee: this.selectNoEmployee.value,
    };

    const getEmirateNameByZone = (name: string) =>
      this.cityList.find((i) => {
        const index = i.place.indexOf(name);
        return index !== -1;
      });

    const getOfficeOrWarehouseSize = (name: string) => {
      if (name.toLowerCase() === 'Warehouse'.toLowerCase())
        return this.spaceTypeForWarehouse?.value;
      if (name.toLowerCase() === 'Office'.toLowerCase())
        return this.spaceTypeForOffice?.value;
      if (name.toLowerCase() === 'Plot'.toLowerCase())
        return this.spaceTypeForPlot?.value;
    };

    let abc = []; // freezone mainland
    if (this.selectedZoneType.mainland && this.selectedZoneType.freezone) {
      abc = ['Freezone', 'Mainland'];
    } else if (this.selectedZoneType.mainland) {
      abc = ['Mainland'];
    } else if (this.selectedZoneType.freezone) {
      abc = ['Freezone'];
    }

    const emirateZones = Object.keys(obj.selectedEmirateZones);

    const arr = [];
    emirateZones.forEach((emirateZone) => {
      TabService.facilitiesRequired.spaceTypeValues.forEach((w) => {
        abc.forEach((a) => {
          arr.push({
            emirate: getEmirateNameByZone(emirateZone).CityName,
            spaceType: w,
            size: getOfficeOrWarehouseSize(w),
            noOfEmployee: this.noOfEmployee?.value,
            noOfVisa: this.noOfVisa?.value,
            zoneType: emirateZone,
            officeType: a,
          });
        });
      });
    });

    console.log('Table Data Check', ':Emirate Zones', emirateZones, arr);

    this.tableData = arr;

    this.calculateCosting();
    this.filterTableData();
    this.initTableCheckboxes();
    this.storeData();
  }

  initTableCheckboxes() {
    const newList: boolean[] = [];
    this.tableData.forEach(() => newList.push(false));
    this.tableCheckboxes.children = newList;
  }

  selectedEmirates: any[] = [];
  getSelectedEmirateIndex = (value: string) => {
    return this.selectedEmirates.indexOf(value);
  };

  onCheckboxChange(e: any) {
    const checkArray: FormArray = this.selectEmirate.get(
      'checkArray'
    ) as FormArray;
    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: any) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }

    const checked = e.target.checked;
    const value = e.target.value;

    if (checked) {
      this.selectedEmirates.push(value);
      this.populateSpaceType();
    } else {
      this.selectedEmirates.splice(this.getSelectedEmirateIndex(value), 1);
      this.populateSpaceType();
    }
    this.storeData();
    this.checkAllEmiratesSelected();
  }

  getEmirateIndex(cityName: string) {
    return this.cityList.findIndex((city) => city.CityName === cityName);
  }

  populateSpaceType() {
    this.companyArea = [];
    const set = {};
    console.log('SelectedEmirates SPace TYpe', this.selectedEmirates);
    this.selectedEmirates.forEach((emirate: string) => {
      const index = this.getEmirateIndex(emirate);
      console.log('EMrirate Index', emirate, index);
      if (index !== -1) {
        set[this.titleCase(this.cityList[index].zone)] = {
          name: this.titleCase(this.cityList[index].zone),
          value: this.titleCase(this.cityList[index].zone),
        };
      }
    });

    this.companyArea = Object.values(set);
    console.log('COmpany Area', this.companyArea);
  }

  selectedZoneType: any = {
    disabled: true,
  };

  onChangeSpaceType(changeEvent: any) {
    const checked = changeEvent.target.checked;
    const value = changeEvent.target.value;

    if (checked) this.selectedZoneType[value.toLowerCase()] = true;
    else this.selectedZoneType[value.toLowerCase()] = false;

    // for disabling Submit button.
    const values = Object.values(
      Object.assign({}, { ...this.selectedZoneType, disabled: undefined })
    );
    let disabled = true;
    console.log(values);
    values.forEach((i) => {
      if (i) disabled = false;
    });
    this.selectedZoneType.disabled = disabled;
    this.checkFreeZoneMainLandSelected();
    this.SelectLocationType.patchValue({ locationType: 'Freezone' });

    this.storeData();
  }

  selectedEmirateZones: any = {};
  selectedEmirateZonesArray: any[] = [];
  onLocationCheckboxChange(e: any) {
    const checkArray: FormArray = this.SelectZoneType.get(
      'zoneArray'
    ) as FormArray;
    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: any) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  handleEmirateZoneChange(changeEvent: any, emirate: any) {
    const checked = changeEvent.target.checked;
    const value = changeEvent.target.value;
    console.log('Value:', value, 'Checked:', checked, 'Emirate:', emirate);
    if (this.selectedEmirateZones[value])
      this.selectedEmirateZones[value] = checked;
    else this.selectedEmirateZones[value] = checked;

    this.selectedEmirateZonesArray = [];
    const entries = Object.entries(this.selectedEmirateZones);
    entries.forEach((keyValuePair) => {
      if (keyValuePair[1]) this.selectedEmirateZonesArray.push(keyValuePair[0]);
    });

    this.getFacilityTypeOfEmirate();
    this.checkSelectAllZoneOrLocation(emirate);

    this.storeData();
  }

  isEmirateZoneChecked(emirate, zone) {}

  // a

  findIndexSpaceType(name: string) {
    console.log('Name', name);
    return TabService.facilitiesRequired.spaceTypeValues.findIndex(
      (item: any) => {
        console.log('Item', item);
        return item.toLowerCase() === name.toLowerCase();
      }
    );
  }

  findSpaceType(name: string) {
    return TabService.facilitiesRequired.spaceTypeValues.find(
      (item: any) => item === name
    );
  }

  handleSpaceTypeValueChange(changeEvent: any, index: number, name: string) {
    // const value = changeEvent.target.checked;
    const indexOfSpaceType = this.findIndexSpaceType(name);
    if (indexOfSpaceType === -1)
      TabService.facilitiesRequired.spaceTypeValues.push(name);
    else
      TabService.facilitiesRequired.spaceTypeValues.splice(indexOfSpaceType, 1);
    this.checkSelectAllPlotOfficeWarehouse();
    this.getFacilityTypeOfEmirate();
    this.storeData();
  }

  getSpaceTypeValues() {
    console.log(TabService.facilitiesRequired.spaceTypeValues);
    return TabService.facilitiesRequired.spaceTypeValues;
  }

  get spaceArray() {
    return this.SelectSpaceype.get('spaceArray');
  }
  get checkArray() {
    return this.selectEmirate.get('checkArray');
  }

  get locationArray() {
    return this.SelectZoneType.get('zoneArray');
  }

  get locationType() {
    return this.SelectLocationType.get('locationType');
  }
  get spaceTypeForWarehouse() {
    return this.SelectSpaceArea.get('spaceTypeForWarehouse');
  }
  get spaceTypeForOffice() {
    return this.SelectSpaceArea.get('spaceTypeForOffice');
  }
  get spaceTypeForPlot() {
    return this.SelectSpaceArea.get('spaceTypeForPlot');
  }
  get zone() {
    return this.SelectLocationType.get('zone');
  }
  edit(step: any) {
    this.isStep = step;
  }
  step1() {
    this.getLocationByCityList(this.checkArray?.value);
    this.checkAllArrayValues = this.checkArray?.value;
    this.isStep += 1;
    this.storeData();
  }

  step3() {
    this.checkAllLocationValues = this.locationArray?.value;
    this.isStep += 1;
    console.log(this.locationArray?.value);
    this.storeData();
  }

  step4() {
    this.isStep += 1;
    // this.spaceTypeArray = this.spaceArray?.value;
    console.log(this.spaceTypeArray);
    this.storeData();
  }
  step5() {
    this.isSpaceAreaAdded();
    this.isStep += 1;
    this.storeData();
  }
  step2() {
    // if (this.locationType?.value == 'Freezone') {
    //   this.isStep += 1;
    // } else {
    //   this.isStep += 2;
    // }

    this.isStep += 1;
    const locationType = this.locationType?.value;
    this.zoneList = [];
    this.locationList = [];
    if (locationType == 'Mainland')
      this.checkAllArrayValues.forEach((emirateName) =>
        this.getLocationByEmirate(emirateName)
      );
    //this.checkALlArrayValue Contains selected Emirates. NEED TO FETCH LOCATIONS

    if (locationType == 'Freezone')
      this.checkAllArrayValues.forEach((emirateName) =>
        this.getZoneByEmirate(emirateName)
      );

    this.storeData();
  }

  getLocationByEmirate(emirateName: string) {
    this.facilityRequiredService
      .getLocationByCity(emirateName)
      .subscribe((res) => {
        if (Array.isArray(res)) {
          this.locationList.push({ name: emirateName, cities: [...res] });
          console.log('Location By Emirates', res);
          this.visaCosting = this.visaCosting.concat(res);
        }
      });
  }

  getZoneByEmirate(emirateName: string) {
    this.facilityRequiredService
      .getZoneByEmirate(emirateName)
      .subscribe((res) => {
        if (Array.isArray(res)) {
          this.zoneList.push({ name: emirateName, zones: [...res] });
          console.log('Zone By Emirates', res);
          this.visaCosting = this.visaCosting.concat(res);
        }
      });
  }

  handleVisaChange(changeEvent: any) {
    // if no of visa required is greater than zero then visaRequired field will change to true else false.
    const value = changeEvent.target.value;
    this.selectNoEmployee.patchValue({ visaRequired: value > 0 });
  }

  save() {
    // this.isStep += 1;
    // this.checkAllArrayValues = this.checkArray?.value;
    // console.log('selectEmirate', this.selectEmirate);
    // console.log('SelectLocationType', this.SelectLocationType);
    // console.log('SelectZoneType', this.SelectZoneType);
    // console.log('SelectSpaceArea', this.SelectSpaceArea);
    // console.log('selectNoEmployee', this.selectNoEmployee);
    // console.log('selectFromTable', this.selectFromTable);

    console.log('Value', this.tableData);
  }

  step7() {
    this.storeData();
    // only selected checkboxes data will be saved
    TabService.formValues.facilitiesRequired = [];
    this.tableCheckboxes.children.forEach((value: boolean, index: number) => {
      if (value) {
        TabService.formValues.facilitiesRequired.push(this.tableData[index]);
      }
    });
    this.route.navigate([this.tabs.navigateToNextPage()]);
  }

  getFacilityTypeList() {
    this.facilityRequiredService.getFacilityType().subscribe((res) => {
      if (Array.isArray(res)) this.facilityTypeCosting = res;
      console.log('Response Facility Type', this.facilityTypeCosting);
    });
  }
  getFacilityTypeListWithCalculateCosting() {
    this.facilityRequiredService.getFacilityType().subscribe((res) => {
      if (Array.isArray(res)) {
        this.facilityTypeCosting = res;
        this.calculateCosting();
      }
      console.log('Response Facility Type', this.facilityTypeCosting);
    });
  }

  facilityTypeList: any[] = [];
  getFacilityTypeOfEmirate() {
    const facilityList = [];

    const findFacilityType = (_id) =>
      facilityList.findIndex((item) => item._id === _id);

    this.selectedEmirateZonesArray.forEach((zone) => {
      this.facilityTypeCosting.forEach((facility) => {
        if (facility.spaceType.toLowerCase() === 'freezone') {
          if (
            zone === facility.freezone &&
            findFacilityType(facility._id) === -1 &&
            this.selectedZoneType.freezone
          ) {
            facility.name = facility.facilityType;
            facility.value = facility.facilityType;
            facilityList.push(facility);
          }
        }
        if (facility.spaceType.toLowerCase() === 'mainland') {
          if (
            zone === facility.mainland &&
            findFacilityType(facility._id) === -1 &&
            this.selectedZoneType.mainland
          ) {
            facility.name = facility.facilityType;
            facility.value = facility.facilityType;
            facilityList.push(facility);
          }
        }
      });

      this.facilityTypeList = this.filterDuplicateFacilityType(facilityList);
    });

    console.log('Facility List After Filter', facilityList);
  }

  filterDuplicateFacilityType(array: any) {
    const newArray = [];

    const findInNewArray = (facilityType) =>
      newArray.findIndex(
        (i) => i.facilityType.toLowerCase() === facilityType.toLowerCase()
      );

    array.forEach((i) => {
      if (findInNewArray(i.facilityType) === -1) {
        newArray.push(i);
      }
    });

    return newArray;
  }

  findFacilityType2(facilityType) {
    const find = this.facilityTypeList.findIndex((i) => {
      console.log(
        'Facility Type CHeck',
        i.facilityType.toLowerCase() === facilityType.toLowerCase(),
        i.facilityType.toLowerCase(),
        facilityType.toLowerCase()
      );
      return i.facilityType.toLowerCase() === facilityType.toLowerCase();
    });
    return find;
  } /// For Office Type Warehouse, Plot

  findFacilityType(
    value: string,
    companyLandType: string,
    emirate: string,
    place: string
  ) {
    const result = this.facilityTypeCosting.find(
      (facility) =>
        facility.facilityType.toLowerCase() == value.toLowerCase() &&
        facility.spaceType.toLowerCase() == companyLandType.toLowerCase() &&
        facility.cityName.toLowerCase() === emirate.toLowerCase() &&
        (companyLandType.toLowerCase() === 'freezone'
          ? facility.freezone.toLowerCase() === place.toLowerCase()
          : facility.mainland.toLowerCase() === place.toLowerCase())
    );
    console.log("CostFindResult", result)
    if (result) return result.totalAnnualRent;
    else return '';
  }

  calculateVisaCosting(zoneType: string, emirate: string) {
    const costing = this.visaCosting.find((item) => {
      console.log(
        'Emirate and ZoneType Incoming',
        'Zone Name',
        item.ZoneName,
        zoneType,
        'Emirate',
        emirate,
        item.cityName
      );
      console.log('Outgoing', item.ZoneName, item.cityName, item);
      return (
        item.ZoneName.toLowerCase() === zoneType.toLowerCase() &&
        item.cityName.toLowerCase() === emirate.toLowerCase()
      );
    });
    console.log('Costing', costing);
    if (costing) return { employeeVisa: 0, investorVisa: 0 };
    return { employeeVisa: 0, investorVisa: 0 };
  }

  calculateCosting() {
    const newArray = [];
    console.log('Table Array Calculate Pricing', this.tableData);
    this.tableData.forEach((item) => {
      console.log('Item For Costing', item);
      const companyLandType = this.locationType?.value; // freezone or Mainland
      const costPerUnit = this.findFacilityType(
        item.spaceType,
        item.officeType.toLowerCase(),
        item.emirate,
        item.zoneType
      );
        if(costPerUnit){
          console.log('FOr Costing', item.spaceType, item.officeType.toLowerCase());
    
          console.log(
            'Item Space Type Size',
            item.spaceType,
            item.size,
            costPerUnit
          );
    
          const totalCost = Number(costPerUnit) * Number(item.size);
          const { employeeVisa, investorVisa } = this.calculateVisaCosting(
            item.zoneType,
            item.emirate
          );
    
          const addVisaCosting = (totalCost: number) =>
            totalCost + item.noOfVisa * employeeVisa;
    
          item.investorVisa = investorVisa;
          item.employeeVisa = employeeVisa;
          item.cost = addVisaCosting(totalCost);
    
          if (!Number.isNaN(item.cost)) newArray.push(item);
        }
    });

    this.tableData = newArray;
    console.log('Table Data With Price:', this.tableData);
    TabService.formValues.facilitiesRequired = this.tableData;
  }

  filterTableData() {
    const array = this.tableData;

    if (this.searchText.length === 0) return (this.filteredTableData = array);
    const result = array.filter(
      (item) =>
        item.emirate.toLowerCase().includes(this.searchText.toLowerCase()) ||
        item.spaceType.toLowerCase().includes(this.searchText.toLowerCase()) ||
        item.officeType.toLowerCase().includes(this.searchText.toLowerCase())
    );
    return (this.filteredTableData = result);
  }

  handleSearchText(changeEvent: any) {
    console.log('Search Value', this.searchText, changeEvent.target.value);
    this.filterTableData();
  }

  handleMainCheckboxChange(changeEvent: any) {
    const checked = changeEvent.target.checked;
    this.tableCheckboxes.children = this.tableCheckboxes.children.map(
      (value) => checked
    );
  }

  toggleDetail(index: number) {
    const currentValue = this.showDetails[index];
    if (currentValue) {
      this.showDetails[index] = false;
    } else {
      this.showDetails[index] = true;
    }
  }

  storeData() {
    const obj = {
      selectedEmirates: this.selectedEmirates,
      selectedEmirateZones: this.selectedEmirateZones,
      selectedEmirateZonesArray: this.selectedEmirateZonesArray,
      selectedZoneType: this.selectedZoneType,
      tableCheckboxes: this.tableCheckboxes,

      selectEmirate: this.selectEmirate.value,
      SelectLocationType: this.SelectLocationType.value,
      SelectZoneType: this.SelectZoneType.value,
      SelectSpaceype: this.SelectSpaceype.value,
      SelectSpaceArea: this.SelectSpaceArea.value,
      selectNoEmployee: this.selectNoEmployee.value,
      isStep: String(this.isStep),
      locationByCityList: this.locationByCityList,
      getLocationList: this.getLocationList,
      // spaceTypeValues: this.spaceTypeValues,

      checkAllArrayValues: this.checkAllArrayValues,
      checkAllLocationValues: this.checkAllLocationValues,
      checkAllSpaceCheckedValues: this.checkAllSpaceCheckedValues,

      locationList: this.locationList,
      zoneList: this.zoneList,

      facilityTypeCosting: this.facilityTypeCosting,

      spaceTypeArray: this.spaceTypeArray,
      companyArea: this.companyArea,
      spaceTypeValues: TabService.facilitiesRequired.spaceTypeValues,

      tableData: this.tableData,
      // searchText: this.searchText,
      // filteredTableData: this.filteredTableData,
      // tableCheckboxes: this.tableCheckboxes,
    };
    console.log('Storing Data', obj);
    sessionStorage.setItem(
      TabService.localStorageKeys.facilitiesRequired,
      JSON.stringify(obj)
    );
  }

  populateData() {
    const business = sessionStorage.getItem(
      TabService.localStorageKeys.businessObjective
    );
    if (business) {
      this.businessObjective = JSON.parse(business);
    }

    const value = sessionStorage.getItem(
      TabService.localStorageKeys.facilitiesRequired
    );

    if (value) {
      const parsed = JSON.parse(value);
      console.log('Parsed Populate', parsed);
      this.selectedEmirates = parsed.selectedEmirates;
      console.log('Parsed Populate2', this.selectedEmirates);
      this.isStep = Number(parsed.isStep);
      this.selectedEmirateZones = parsed.selectedEmirateZones;
      this.selectedEmirateZonesArray = parsed.selectedEmirateZonesArray;
      this.selectedZoneType = parsed.selectedZoneType;
      this.tableCheckboxes = parsed.tableCheckboxes;
      this.tableData = parsed.tableData;
      this.SelectLocationType.patchValue({ ...parsed.SelectLocationType });
      this.SelectZoneType.patchValue({ ...parsed.SelectZoneType });
      this.SelectSpaceype.patchValue({ ...parsed.SelectSpaceype });
      this.SelectSpaceArea.patchValue({ ...parsed.SelectSpaceArea });
      this.selectNoEmployee.patchValue({ ...parsed.selectNoEmployee });
      this.companyArea = parsed.companyArea;

      TabService.facilitiesRequired.spaceTypeValues = parsed.spaceTypeValues;
      // this.selectFromTable.patchValue({...parsed.selectFromTable})

      const selectEmirateValue = parsed.selectEmirate;
      console.log('this.selectEmirate', selectEmirateValue);
      this.checkArray.patchValue(selectEmirateValue.checkArray);
      this.locationByCityList = parsed.locationByCityList;
      this.getLocationList = parsed.getLocationList;
      this.spaceTypeValues = parsed.spaceTypeValues;

      this.checkAllArrayValues = parsed.checkAllArrayValues;
      this.checkAllLocationValues = parsed.checkAllLocationValues;
      this.checkAllSpaceCheckedValues = parsed.checkAllSpaceCheckedValues;

      this.locationList = parsed.locationList;
      this.zoneList = parsed.zoneList;

      this.facilityTypeCosting = parsed.facilityTypeCosting;

      this.spaceTypeArray = parsed.spaceTypeArray;
    }
    this.allCity();
    this.getLocation();
    this.getFacilityTypeListWithCalculateCosting();
    this.filterTableData();

    this.checkAllEmiratesSelected();
    this.populateSpaceType();
    console.log('Populate SPaceType');
    if (this.selectedEmirates.length) {
      this.getLocationByCityList(this.selectedEmirates);
    }

    this.getFacilityTypeOfEmirate();
    this.isSpaceAreaAdded();
  }

  handleTableCheckboxChange(changeEvent: any, index: number) {
    const checked = changeEvent.target.checked;
    const selected = this.tableData[index];

    if (checked) {
      this.tableData = this.tableData.map((item) => {
        item.isDisabled =
          selected.emirate !== item.emirate ||
          selected.officeType.toLowerCase() !== item.officeType.toLowerCase() ||
          selected.zoneType !== item.zoneType;
        return item;
      });
    }
    if (!checked) {
      this.tableData = this.tableData.map((item) => {
        item.isDisabled = false;
        return item;
      });
    }
  }

  // for select all;
  checkSelectedEmirate(city: string) {
    return -1 < this.selectedEmirates.indexOf(city);
  }

  checkAllEmiratesSelected() {
    if (this.selectedEmirates.length === this.cityList.length)
      this.emirateSelectedAll = true;
    else this.emirateSelectedAll = false;

    console.log(
      'Check ALl Emirate Selected Called',
      this.selectedEmirates.length,
      this.cityList.length
    );
  }

  handleSelectAllEmirateChange(changeEvent: any) {
    const checked = changeEvent.target.checked;

    const checkArray: FormArray = this.selectEmirate.get(
      'checkArray'
    ) as FormArray;

    checkArray.controls.forEach((item: any, index) => {
      checkArray.removeAt(index);
    });
    this.selectedEmirates = [];
    console.log('Emirate Checked', checked);

    if (checked) {
      this.selectedEmirates = [];
      this.cityList.forEach((i) => {
        checkArray.push(new FormControl(i.CityName));
        this.selectedEmirates.push(i.CityName);
      });
    } else {
      checkArray.controls.forEach((item: any, index) => {
        checkArray.removeAt(index);
      });
      this.selectedEmirates = [];
    }
    console.log('Check Array', checkArray.value);
    this.populateSpaceType();
  }

  checkFreeZoneMainLandSelected() {
    const values = Object.values(
      Object.assign({}, { ...this.selectedZoneType, disabled: undefined })
    );
    let disabled = 0;
    console.log(values);
    values.forEach((i) => {
      if (i) disabled += 1;
    });
    this.freeZoneMainLandSelectedAll = disabled === this.companyArea.length;
  }

  handleSelectAllMainLandFreeZoneChange(changeEvent: any) {
    const checked = changeEvent.target.checked;

    if (checked) {
      this.companyArea.forEach((i) => {
        this.selectedZoneType[i.name.toLowerCase()] = true;
      });
    } else {
      this.companyArea.forEach((i) => {
        this.selectedZoneType[i.name.toLowerCase()] = false;
      });
    }

    const values = Object.values(
      Object.assign({}, { ...this.selectedZoneType, disabled: undefined })
    );
    let disabled = true;
    console.log(values);
    values.forEach((i) => {
      if (i) disabled = false;
    });
    this.selectedZoneType.disabled = disabled;
    this.SelectLocationType.patchValue({ locationType: 'Freezone' });
    this.storeData();
  }

  allLocationOrZoneSelected: any[] = [];

  checkSelectAllZoneOrLocation(emirate: string) {
    const list = this.getZoneListByEmirate(emirate);
    let allSelected = true;
    list.forEach((i) => {
      if (!this.selectedEmirateZones[i.ZoneName]) allSelected = false;
    });
    this.allLocationOrZoneSelected[emirate] = allSelected;
  }

  handleSelectAllZoneOrLocation(changeEvent: any, emirate: string) {
    const checked = changeEvent.target.checked;

    if (checked) {
      this.getZoneListByEmirate(emirate).forEach((i) => {
        this.selectedEmirateZones[i.ZoneName] = true;
      });
    } else {
      this.getZoneListByEmirate(emirate).forEach((i) => {
        this.selectedEmirateZones[i.ZoneName] = false;
      });
    }
    this.selectedEmirateZonesArray = [];
    const entries = Object.entries(this.selectedEmirateZones);
    entries.forEach((keyValuePair) => {
      if (keyValuePair[1]) this.selectedEmirateZonesArray.push(keyValuePair[0]);
    });

    this.getFacilityTypeOfEmirate();
  }

  selectAllPlotWarehouseOffice: boolean = false;

  checkSelectAllPlotOfficeWarehouse() {
    this.selectAllPlotWarehouseOffice =
      this.facilityTypeList.length ===
      TabService.facilitiesRequired.spaceTypeValues.length;
  }

  handleSelectAllPlotOfficeWarehouse(changeEvent: any) {
    const checked = changeEvent.target.checked;
    TabService.facilitiesRequired.spaceTypeValues = [];
    if (checked) {
      this.facilityTypeList.forEach((i) => {
        TabService.facilitiesRequired.spaceTypeValues.push(i.name);
      });
    } else {
      TabService.facilitiesRequired.spaceTypeValues = [];
    }
  }

  isValidSpaceAreaSubmitButton: boolean = true
  isSpaceAreaAdded(){
    const values = this.SelectSpaceArea.value;
    if(this.findIndexSpaceType('Warehouse') !== -1) return this.isValidSpaceAreaSubmitButton = typeof values.spaceTypeForWarehouse !== 'number'
    if(this.findIndexSpaceType('Plot') !== -1) return this.isValidSpaceAreaSubmitButton = typeof values.spaceTypeForPlot !== 'number'
    if(this.findIndexSpaceType('Office') !== -1) return this.isValidSpaceAreaSubmitButton = typeof values.spaceTypeForOffice !== 'number'
    // [Log] SPace Area Values – {spaceTypeForOffice: "", : 1000, spaceTypeForPlot: ""} (src_app_modules_business_business_module_ts.js, line 35922)
    return this.isValidSpaceAreaSubmitButton = false;
  }
}
