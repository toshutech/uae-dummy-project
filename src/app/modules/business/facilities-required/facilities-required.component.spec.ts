import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitiesRequiredComponent } from './facilities-required.component';

describe('FacilitiesRequiredComponent', () => {
  let component: FacilitiesRequiredComponent;
  let fixture: ComponentFixture<FacilitiesRequiredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FacilitiesRequiredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitiesRequiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
