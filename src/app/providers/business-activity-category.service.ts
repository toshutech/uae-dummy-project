import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BusinessActivityCategoryService {
  apiUrl: string = environment.apiUrl;
  constructor(private http: HttpClient) { }

  getAllBusiness() {
    return this.http.get(this.apiUrl + 'activity-category')
  }
  getBusinessCategory() {
    return this.http.get(this.apiUrl + 'business-activities')
  }
  getBusinessCategoryByCategoryName(category: any) {
    return this.http.get(this.apiUrl + 'business-activities/' + category)
  }

}
