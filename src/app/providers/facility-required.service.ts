import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FacilityRequiredService {
  apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAllFacilityRequired() {
    return this.http.get(this.apiUrl + 'city')
  }

  getEmiratesBasedOfActivity(businessActivity:any) {
    return this.http.post(this.apiUrl + 'associate-type/business', { activity: businessActivity })
  }

  getAllFacilityLocation() {
    return this.http.get(this.apiUrl + 'location')
  }
  getLocationByCity(city: any) {
    return this.http.get(this.apiUrl + 'location/' + city)
  }

  getZoneByEmirate(emirateName:string){
    return this.http.get(this.apiUrl + 'zone/' + emirateName)
  }

  getPartnershipType(){
    return this.http.get(this.apiUrl + 'partnership')
  }

  getDocumentTypes(){
    return this.http.get(this.apiUrl + 'document')
  }

  getFacilityType(){
    return this.http.get(this.apiUrl + 'facility-type')
  }

  getSetupDetails({ cityName, spaceType, spaceTypeName }){
    return this.http.get(this.apiUrl + `setupCost?cityName=${cityName}&spaceType=${spaceType}&spaceTypeName=${spaceTypeName}`)
  }

  getLicenseCost({ spaceType, zoneType,emirate }){
    return this.http.get(this.apiUrl + `licence-cost?zoneType=${zoneType}&spaceType=${spaceType}&emirate=${emirate}`)
  }

  getEmployeeInvestorVisaCost(type:string, place:string, emirate: string){
    console.log("FInalCosting", type, place, emirate)
    if(type === 'regulatoryAuthority'){
      return this.http.post(this.apiUrl + `zone/cost`, { place, emirate});
    } else {
      return this.http.post(this.apiUrl + `location/cost`, { place, emirate });
    }
  }
}
