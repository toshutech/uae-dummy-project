import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

export interface BusinessObjective {
  businessActivityCategory: string;
  businessActivity: any[];
}

export interface InvestorInformation {
  partnership: string,
  investors : any[],
  amount: number
}

export interface FormValueTypes {
  businessName: any;
  businessObjective: BusinessObjective;
  facilitiesRequired: any[];
  investorInformation: InvestorInformation;
  requiredDocuments: any;
}

export const INITIAL_STATE = {
  businessName: {},
  businessObjective: {
    businessActivityCategory: '',
  businessActivity: []
  },
  facilitiesRequired: [],
  investorInformation: {
    partnership: '',
    investors : [],
    amount: 0
  },
  requiredDocuments: {}
}

@Injectable({
  providedIn: 'root',
})
export class TabService {
  tabsNumber = 1;
  static tabsNumber = 0;
  static facilitiesRequired: any = {
    spaceTypeValues: [],
  };
  currentTabIndex: number = 0;
  routeString: any[] = [];


  static formValues: FormValueTypes = INITIAL_STATE

  static localStorageKeys = {
    businessName: "editBusinessName",
    businessObjective: "editBusinessObjective",
    facilitiesRequired: "editFacilitiesRequired",
    investorInformation: "editInvestorInformation",
    requiredDocument: "editRequiredDocument",
    
    activeTabIndex: "activeTabIndex",
    businessActivity: "businessActivity",

    globalDataKey: "globalDataKey"
  }

  constructor(private http: HttpClient) {
    this.routeString = [
      '/business/business-objective',
      '/business/facilites',
      '/business/investor-info',
      '/business/business-name',
      '/business/required-doc',
      '/business/summary',
    ];
    TabService.formValues = {
      businessObjective:{
        businessActivityCategory: "",
        businessActivity: [],
      },
      businessName: {},
      facilitiesRequired: [],
      investorInformation: {
        partnership: "",
        investors: [],
        amount: 0
      },
      requiredDocuments: {}
    }

    const tabNumber = sessionStorage.getItem('tabNumber');
    if(tabNumber){
      TabService.tabsNumber = Number(tabNumber);
      this.currentTabIndex = Number(tabNumber);
      console.log("Tab Number",tabNumber);
    } 

    this.getValue();
  }

  navigateToNextPage() {
    if (this.routeString.length - 1 === TabService.tabsNumber) return;
    console.log('TabService', TabService.formValues)
    const newTab = TabService.tabsNumber + 1;
    TabService.tabsNumber = newTab;
    this.currentTabIndex = newTab;
    console.log('RouteString', this.routeString[newTab], newTab);
    // callback([this.routeString[newTab]]);
    this.storeValue();
    sessionStorage.setItem('tabNumber', `${this.currentTabIndex}`)
    return this.routeString[newTab];
  }

  navigateToPage(){
    return this.routeString[this.currentTabIndex]
  }

  storeValue(){
    const obj = {
      ...TabService.formValues
    }
    sessionStorage.setItem(TabService.localStorageKeys.globalDataKey, JSON.stringify(obj))
  }

  getValue(){
    const data = sessionStorage.getItem(TabService.localStorageKeys.globalDataKey)
    if(data){
      const parsed = JSON.parse(data);
      TabService.formValues = parsed;
    }
  }

  navigateToParticularPage(tabNum:number){
    // if (this.routeString.length - 1 === TabService.tabsNumber) return;
    console.log('TabService', TabService.formValues)
    const newTab = tabNum;
    TabService.tabsNumber = newTab;
    this.currentTabIndex = newTab;
    console.log('RouteString', this.routeString[newTab], newTab);
    // callback([this.routeString[newTab]]);
    sessionStorage.setItem('tabNumber', `${this.currentTabIndex}`)
    return this.routeString[newTab];
  }

}
