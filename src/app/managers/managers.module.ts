import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagersRoutingModule } from './managers-routing.module';
import { ManagersComponent } from '../managers.component';
import { ManagerComponent } from './manager/manager.component';
import { ManagerEditComponent } from './manager-edit/manager-edit.component';
import { ManagerButtonComponent } from './shared/manager-button/manager-button.component';


@NgModule({
  declarations: [
    ManagersComponent,
    ManagerComponent,
    ManagerEditComponent,
    ManagerButtonComponent
  ],
  imports: [
    CommonModule,
    ManagersRoutingModule
  ]
})
export class ManagersModule { }
