import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path: 'business',
    loadChildren: () =>
      import('./modules/business/business.module').then((m) => m.BusinessModule)
  },
  {
    path: 'pages',
    loadChildren: () =>
      import('./modules/pages/pages.module').then((m) => m.PagesModule)
  },
  {
    path: '',
    redirectTo: '/pages/home',
    pathMatch: 'full'
  }
  // {
  //   path:'shared',
  //   loadChildren:()=>
  //   import('./shared/shared.module').then((m)=>m.SharedModule)
  // }





];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
