import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomersComponent } from './customers.component';
import { CustomerComponent } from './customer/customer.component';
import { CustomerEditComponent } from './customer-edit/customer-edit.component';
import { CustomerButtonComponent } from './shared/customer-button/customer-button.component';


@NgModule({
  declarations: [
    CustomersComponent,
    CustomerComponent,
    CustomerEditComponent,
    CustomerButtonComponent
  ],
  imports: [
    CommonModule,
    CustomersRoutingModule
  ]
})
export class CustomersModule { }
